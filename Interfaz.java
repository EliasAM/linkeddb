package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;

public class Interfaz {

    public Button crear_JSONStore;
    public Button crear_ventana;
    public Button crear_ventana2;
    public Button crear_ventana3;
    public TreeView<String> gran_arbol;
    public AnchorPane anchorPane;
    private TreeItem<String> seleccionado;
    private String seleccionado_nom;
    private int contador;
    private TreeItem<String> root;
    private TreeItem<String> itemChild;
    private Lista_Enlazada primerStore;


    public Interfaz(){
        contador = 0;
    }
    public void hablar(ActionEvent actionEvent) {
        System.out.println("holaaa");
    }

    private void segundaVentana() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ventana_3.fxml"));
        Parent root1 = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.show();
    }

    public void segunda_ventana(ActionEvent actionEvent) throws IOException {
        segundaVentana();
    }

    public void crear_JSONStore(ActionEvent actionEvent) throws IOException {
        if (contador == 0) {
            root = new TreeItem<>("JSONStores");
            primerStore = new Lista_Enlazada();
        }
        createTree();
    }

    private void createTree(String... rootItems) throws IOException {
        if (contador == 0) {
            contador ++;
            String respuesta = JOptionPane.showInputDialog("¿Nombre del JSONStore?");
            //root.setExpanded(true);
            //create child
            itemChild = new TreeItem<>(respuesta);
            itemChild.setExpanded(true);
            //root is the parent of itemChild
            root.getChildren().add(itemChild);
            primerStore.agregarPrincipio(respuesta);
        }else {
            crear_ventana.setVisible(true);
            String respuesta = JOptionPane.showInputDialog("¿Nombre del JSONStore?");
            TreeItem<String> itemChild_2 = new TreeItem<>(respuesta);
            itemChild_2.setExpanded(true);
            root.getChildren().add(itemChild_2);
            primerStore.agregarPrincipio(respuesta);
        }
        gran_arbol.setRoot(root);

    }

    @FXML
    private void crear_Documento(ActionEvent actionEvent) throws IOException {
        String respuesta = JOptionPane.showInputDialog("¿Nombre del DocumentoJSON?");
        segundaVentana();
        TreeItem<String> itemChild_2 = new TreeItem<>(respuesta);
        seleccionado.getChildren().add(itemChild_2);
        Nodo_Enlazado documento = primerStore.buscar_Documento(seleccionado_nom);
        documento.crear_doc(respuesta);
        System.out.println(documento.toString());
    }


    public void clicked(MouseEvent mouseEvent) {
        TreeItem<String> item = gran_arbol.getSelectionModel().getSelectedItem();
        try {
            System.out.println(item.getValue());
            seleccionado_nom = item.getValue();
            seleccionado = item;
            crear_ventana.setVisible(true);
            crear_ventana2.setVisible(true);
            crear_ventana3.setVisible(true);
            System.out.println(primerStore.toString());
            if (seleccionado == root){
                crear_ventana.setVisible(false);
                crear_ventana2.setVisible(false);
                crear_ventana3.setVisible(false);
            }
        }catch (Exception ignore){
        }
    }


}
