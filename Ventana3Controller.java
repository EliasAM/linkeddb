package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Ventana3Controller {
    public TextField nombreAtributo;
    public Label verifierEject;
    public ChoiceBox<String> tipoAtributo;
    private ObservableList<String> atributos = FXCollections.observableArrayList("entero", "flotante", "cadena", "fecha-hora");


    public Ventana3Controller(){
    }

    public void crearDocumento(){
        System.out.println("hola");
        verifierEject.setText(nombreAtributo.getText());
        System.out.println(nombreAtributo.getText());
    }

    public void seleccionarTipo(){

        tipoAtributo.setValue("entero");
        tipoAtributo.setItems(atributos);
    }
}
