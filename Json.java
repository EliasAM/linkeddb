package sample;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

class Json {
    private static JSONObject objeto;
    private static String nombre;
    private static String atributo;
    private static String requerido;
    private static String llave;
    private static String vdefecto;

    public static void Json(String nombre) throws JSONException {
        Lista_Simple lista = new Lista_Simple();
        Nodo_Enlazado nodo = new Nodo_Enlazado(nombre);
        lista.addFirst(nodo);
        objeto = new JSONObject();
        objeto.put("Nombre", nombre);
        crear_atributo(nombre);
        crear_carpeta(nombre);
    }

    public static void crear_atributo(String nombre_doc){
        JSONObject atributo_nuevo = new JSONObject();
        nombre = JOptionPane.showInputDialog("Ingrese nombre del atributo: ");
        atributo = JOptionPane.showInputDialog("Ingrese tipo de atributo: ");
        llave = JOptionPane.showInputDialog("Ingrese tipo especial: ");
        requerido = JOptionPane.showInputDialog("Ingrese si es requerido o no: ");
        vdefecto = JOptionPane.showInputDialog("Ingrese valor por defecto: ");

    }

    public static void guardar_datos(String tipo_dato, String dato) throws IOException, JSONException {
        objeto.put(tipo_dato, dato);
    }

    private static void crear_carpeta(String nombre) {
        new File("C:\\Users\\satellite\\Desktop\\" + nombre);
    }
}