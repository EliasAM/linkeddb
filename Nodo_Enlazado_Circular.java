package sample;

public class Nodo_Enlazado_Circular {
    private Object dato;
    public Nodo_Enlazado_Circular siguiente;
    public Nodo_Enlazado_Circular anterior;


    public Nodo_Enlazado_Circular() {
        siguiente = null;
        anterior = null;
    }


    public Nodo_Enlazado_Circular(Object ingreso) {
        anterior = null;
        siguiente = null;
        dato = ingreso;
    }


    public Nodo_Enlazado_Circular(Object ingreso, Nodo_Enlazado_Circular siguiente, Nodo_Enlazado_Circular anterior) {
        this.anterior = anterior;
        this.siguiente = siguiente;
        dato = ingreso;
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public Nodo_Enlazado_Circular getSiguiente() {
        return siguiente;
    }

    public Nodo_Enlazado_Circular getAnterior() {
        return anterior;
    }
}
