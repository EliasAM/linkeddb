package sample;

public class Nodo_Enlazado {
    private String dato;
    public Nodo_Enlazado siguiente;
    public Nodo_Enlazado anterior;
    public Lista_Enlazada_Circular documentos;


    public Nodo_Enlazado(String ingreso) {
        anterior = null;
        siguiente = null;
        dato = ingreso;
        documentos = new Lista_Enlazada_Circular(ingreso);
    }


    public Nodo_Enlazado(String ingreso, Nodo_Enlazado siguiente, Nodo_Enlazado anterior) {
        this.anterior = anterior;
        this.siguiente = siguiente;
        dato = ingreso;
    }

    public Nodo_Enlazado() {
    }


    public void crear_doc(String nombre_doc) {
        documentos.addFirst(nombre_doc);
    }

    public String toString(){
        return documentos.toString();
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public Nodo_Enlazado getSiguiente() {
        return siguiente;
    }

    public Nodo_Enlazado getAnterior() {
        return anterior;
    }
}
