package sample;


import java.util.Objects;

public class Lista_Enlazada extends Nodo_Enlazado {
    Nodo_Enlazado inicio, fin;

    public Lista_Enlazada() {
        inicio = fin = null;
    }

    // agregar un nodo al final
    public void agregarFinal(String n) {
        Nodo_Enlazado nodo = new Nodo_Enlazado(n);
        nodo.siguiente = null;
        if (inicio == null)
            inicio = nodo;
        else {
            nodo.anterior = fin;
            fin.siguiente = nodo;
        }
        fin = nodo;
    }

    // agregar un nodo al principio
    public void agregarPrincipio(String n) {
        Nodo_Enlazado nodo = new Nodo_Enlazado(n);
        nodo.anterior = null;
        if (inicio == null)
            fin = nodo;
        else {
            nodo.siguiente = inicio;
            inicio.anterior = nodo;
        }
        inicio = nodo;
    }

    // eliminar un nodo
    public void eliminar(Nodo_Enlazado n) {
        if (n == inicio)
            inicio = n.siguiente;
        else if (n == fin)
            fin = fin.anterior;
        else {
            n.anterior.siguiente = n.siguiente;
            n.siguiente.anterior = n.anterior;
        }
    }

    // elimminar por indice
    public void eliminarPorIndice(int numero) {
        int indice = 0;
        Nodo_Enlazado nodo = inicio;
        while (indice != numero){
            if (nodo.siguiente == null){
                System.out.println("El indice no es valido");
                return;
            }
            else {
                indice += 1;
                nodo.siguiente = nodo;
            }
        }
        eliminar(nodo);
    }

    public Nodo_Enlazado buscar_Documento(String buscado){
        Nodo_Enlazado aux = inicio;

        while (!Objects.equals(aux.getDato().toString(), buscado)){
            aux = aux.siguiente;
        }
        return aux;
    }
    
    public String toString(){
        Nodo_Enlazado aux = inicio;
        String resultado = "";

        while (aux.siguiente != null){
            resultado += aux.getDato() + ", ";
            aux = aux.siguiente;
        }
        resultado += aux.getDato() + ".";
        return "Lista:" + resultado;
    }
}