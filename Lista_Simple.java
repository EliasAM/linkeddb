package sample;

public class Lista_Simple extends Nodo_Simple{

    private Nodo_Simple inicio;
    private Nodo_Simple fin;

    public boolean isEmpty() {
        return (inicio == null);
    }

    public Object obtenerInicio(){
        return inicio.getDato();
    }

    public Object obtenerFinal(){
        return fin.getDato();
    }

    public void addFirst(Object item) {
        Nodo_Simple node = new Nodo_Simple();
        node.setDato(item);

        if (this.isEmpty()) { // no items in list
            inicio = node;
            fin = node;
        } else { // one or more items in list
            node.siguiente = inicio;
            inicio = node;
        }
    }

    public Object removeFirst() {
        if (this.isEmpty()) { //no nodes in list
            return null;
        } else if (inicio == fin) { // one node in list
            Object item = inicio.getDato();
            inicio = null;
            fin = null;
            return item;
        } else { // more than one node in list
            Object item = inicio.getDato();
            inicio = inicio.siguiente;
            return item;
        }
    }

    public void addLast(Object item) {
        Nodo_Simple node = new Nodo_Simple();
        node.setDato(item);

        if (this.isEmpty()) { // no items in list
            inicio =  node;
            fin = node;
        } else { // one or more items in list
            fin.siguiente = node;
            fin = node;
        }
    }

    public Object removeLast() {
        if (this.isEmpty()) {
            return null; //there is no last element
        } else if (inicio == fin) { // one element in list
            Object item = inicio.getDato();
            inicio = null;
            fin = null;
            return item;
        } else { // more than one item in list
            Nodo_Simple n = inicio; //and we already know that first and second is not null

            // we need a link to the node previous to the last node, so...
            while (n.siguiente.siguiente != null) {
                n = n.siguiente;
            }
            //n is now parent to last node...
            Object item = n.siguiente.getDato();
            n.siguiente = null;
            // the above would have been much easier in a doubly-linked list,
            // where we have a link to the previous node!

            //don't forget to update "last" before we return the item removed...
            fin = n;
            return item;
        }
    }

    public boolean contains(Object item) {
        for (Nodo_Simple n = inicio; n != null; n = n.siguiente) {
            if (n.getDato().equals(item)) return true;
        }
        return false;
    }

    public String toString() {
        Nodo_Simple i = new Nodo_Simple();
        i = inicio;
        String outputStr = "";
        outputStr += "First: " + ((inicio != null) ? inicio.getDato() : "-") + "\t";
        outputStr += "Last: " + ((fin != null) ? fin.getDato() : "-") + "\t\t";
        outputStr += " List: ";
        while (inicio != i) {
            outputStr += i + " ";
        }
        outputStr += i;
        return outputStr;
    }

}
