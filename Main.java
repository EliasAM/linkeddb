package sample;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main extends Application {


    public static void main(String[] args) throws IOException {
        launch(args);
        String respuesta = JOptionPane.showInputDialog("¿Nuevo JSONStore?");
        System.out.println(respuesta);
        switch (respuesta) {
            case "si":
                String respuesta_2 = JOptionPane.showInputDialog("¿Nombre del JSONStore?");
                File file = new File("C:\\Users\\satellite\\IdeaProjects\\Programa5\\src\\" + respuesta_2);
                System.out.println(file.mkdir());
                System.out.println(file.createNewFile());
                String respuesta_3 = JOptionPane.showInputDialog("¿Nombre del DocumentoJSON?");
                FileWriter file2 = new FileWriter("C:\\Users\\satellite\\IdeaProjects\\Programa5\\src\\" + respuesta_2 + "\\" + respuesta_3 + ".txt");
        }
        Lista_Enlazada_Circular list = new Lista_Enlazada_Circular("1");
        list.addFirst("a");
        list.addFirst("b");
        list.addFirst("c");
        list.addFirst("d");
        list.addFirst("e");
        System.out.println(list);
        System.out.println(list.obtenerInicio());
        System.out.println(list.obtenerFinal());
        System.out.println("contains c? " + list.contains("c"));

        Lista_Enlazada_Circular list_2 = new Lista_Enlazada_Circular("2");
        list_2.addFirst("a");
        Lista_Enlazada_Circular list_3 = new Lista_Enlazada_Circular("3");
        list_3.addFirst("Elías");

        Lista_Enlazada lista = new Lista_Enlazada();
        lista.agregarPrincipio("1");
        lista.agregarFinal("2");
        lista.agregarPrincipio("3");
        System.out.println(lista.toString());


    }

    public static void crear_Json() throws IOException {
        String respuesta = JOptionPane.showInputDialog("¿Nuevo JSONStore?");
        System.out.println(respuesta);
        switch (respuesta) {
            case "si":
                String respuesta_2 = JOptionPane.showInputDialog("¿Nombre del JSONStore?");
                File file = new File("C:\\Users\\satellite\\IdeaProjects\\Programa5\\src\\" + respuesta_2);
                System.out.println(file.mkdir());
                System.out.println(file.createNewFile());
                String respuesta_3 = JOptionPane.showInputDialog("¿Nombre del DocumentoJSON?");
                FileWriter file2 = new FileWriter("C:\\Users\\satellite\\IdeaProjects\\Programa5\\src\\" + respuesta_2 + "\\" + respuesta_3 + ".txt");
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("LinkedDB");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}

