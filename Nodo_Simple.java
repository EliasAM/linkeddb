package sample;

public class Nodo_Simple {
    private Object dato;
    public Nodo_Simple siguiente;


    public Nodo_Simple() {
        siguiente = null;
    }


    public Nodo_Simple(Object ingreso) {
        siguiente = null;
        dato = ingreso;
    }


    public Nodo_Simple(Object ingreso, Nodo_Simple siguiente) {
        this.siguiente = siguiente;
        dato = ingreso;
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public Nodo_Simple getSiguiente() {
        return siguiente;
    }
}
