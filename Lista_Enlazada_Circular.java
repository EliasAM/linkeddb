package sample;

import java.util.Iterator;

public class Lista_Enlazada_Circular extends Nodo_Enlazado_Circular  implements Iterable {


    // instance variables
    private Nodo_Enlazado_Circular inicio;
    private Nodo_Enlazado_Circular fin;
    public String nombre_docu;

    public Lista_Enlazada_Circular(String ingreso) {
        nombre_docu = ingreso;
    }

    public Lista_Enlazada_Circular(Lista_Enlazada_Circular lista){

    }

    public boolean isEmpty() {
        return (inicio == null);
    }

    public Object obtenerInicio(){
        return inicio.getDato();
    }

    public Object obtenerFinal(){
        return fin.getDato();
    }

    public void addFirst(String item) {
        Nodo_Enlazado_Circular node = new Nodo_Enlazado_Circular();
        node.setDato(item);

        if (this.isEmpty()) { // no items in list
            inicio = node;
            fin = node;
        } else { // one or more items in list
            node.siguiente = inicio;
            node.anterior = fin;
            inicio = node;
        }
    }

    public Object removeFirst() {
        if (this.isEmpty()) { //no nodes in list
            return null;
        } else if (inicio == fin) { // one node in list
            Object item = inicio.getDato();
            inicio = null;
            fin = null;
            return item;
        } else { // more than one node in list
            Object item = inicio.getDato();
            inicio = inicio.siguiente;
            return item;
        }
    }

    public void addLast(String item) {
        Nodo_Enlazado_Circular node = new Nodo_Enlazado_Circular();
        node.setDato(item);

        if (this.isEmpty()) { // no items in list
            inicio = node;
            fin = node;
        } else { // one or more items in list
            fin.siguiente = node;
            fin = node;
        }
    }

    public Object removeLast() {
        if (this.isEmpty()) {
            return null; //there is no last element
        } else if (inicio == fin) { // one element in list
            Object item = inicio.getDato();
            inicio = null;
            fin = null;
            return item;
        } else { // more than one item in list
            Nodo_Enlazado_Circular n = inicio; //and we already know that first and second is not null

            // we need a link to the node previous to the last node, so...
            while (n.siguiente.siguiente != null) {
                n = n.siguiente;
            }
            //n is now parent to last node...
            Object item = n.siguiente.getDato();
            n.siguiente = null;
            // the above would have been much easier in a doubly-linked list,
            // where we have a link to the previous node!

            //don't forget to update "last" before we return the item removed...
            fin = n;
            return item;
        }
    }
    public void agregar_Documento(){

    }

    public boolean contains(Object item) {
        for (Nodo_Enlazado_Circular n = inicio; n != null; n = n.siguiente) {
            if (n.getDato().equals(item)) return true;
        }
        return false;
    }

    private class TopToBottomIterator implements Iterator {
        Nodo_Enlazado_Circular nodo = inicio;

        public boolean hasNext() {
            return (nodo != null);
        }

        public Object next() {
            Object itemToReturn = nodo.getDato();
            nodo = nodo.siguiente;
            return itemToReturn;
        }
    }

    public Iterator iterator() {
        return new TopToBottomIterator();
    }

    public String toString() {
        Nodo_Enlazado_Circular aux = inicio;
        String resultado = "";

        while (aux.siguiente != null){
            resultado += aux.getDato() + ", ";
            aux = aux.siguiente;
        }
        resultado += aux.getDato() + ".";
        return "Lista" + nombre_docu + ": "+ resultado;
    }
}
